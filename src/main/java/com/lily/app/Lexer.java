package com.lily.app;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;
import com.lily.app.Token;
import org.javatuples.Pair;

public class Lexer {
  public static Stream<Token> lex(String input) {
    return Stream.iterate(Pair.with(input, (Token)null), new TokenExtractor())
      .map(Pair::getValue1)
      .skip(1)
      .takeWhile(t -> !t.isEof());
  }
}

class TokenExtractor implements UnaryOperator<Pair<String, Token>> {
  static List<String> keywords = Arrays.asList("if", "else", "true", "false", "return", "==", "=", "!=", "<=", ">=", "<", ">", "&&", "||", "%", "!", "+", "-", "*", "/", "(", ")", "{", "}", ";");

  @Override
  public Pair<String, Token> apply(Pair<String, Token> input) {
    String s = input.getValue0();

    // skip whitespaces
    while (!s.isEmpty() && Character.isWhitespace(s.charAt(0))) {
      s = s.substring(1);
    }

    if (s.isEmpty()) {
      return Pair.with("", Token.newEof());
    }

    // parse number
    if (Character.isDigit(s.charAt(0))) {
      int value = 0;
      while (Character.isDigit(s.charAt(0))) {
        value = value * 10 + (s.charAt(0) - '0');
        s = s.substring(1);
      }
      return Pair.with(s, Token.newNumber(value));
    }

    // parse keyword
    final String tmp = s;
    Optional<String> keyword = keywords.stream().filter(k -> tmp.startsWith(k)).findAny();

    if (keyword.isPresent()) {
      s = s.substring(keyword.get().length());
      return Pair.with(s, Token.newKeyword(keyword.get()));
    }

    // parse identifier
    if (Character.isLetter(s.charAt(0)) || s.charAt(0) == '_') {
      String value = "";
      while (!s.isEmpty() && (Character.isLetterOrDigit(s.charAt(0)) || s.charAt(0) == '_')) {
        value += s.charAt(0);
        s = s.substring(1);
      }
      return Pair.with(s, Token.newIdentifier(value));
    }

    throw new RuntimeException("unexpected character: " + s.charAt(0));
  }
}
