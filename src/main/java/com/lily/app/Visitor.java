package com.lily.app;

import com.lily.app.ast.Statement;
import com.lily.app.ast.Expression;

public interface Visitor {
  public abstract void visit(Statement.If statement);
  public abstract void visit(Statement.Return statement);
  public abstract void visit(Statement.ExpressionStatement statement);
  public abstract void visit(Statement.Declaration statement);
  public abstract void visit(Statement.Function statement);
  public abstract void visit(Statement.Assignment assignment);
  public abstract void visit(Expression.Binary expression);
  public abstract void visit(Expression.Number expression);
  public abstract void visit(Expression.Variable expression);
  public abstract void visit(Expression.Call expression);
}
