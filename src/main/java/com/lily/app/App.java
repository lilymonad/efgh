package com.lily.app;

import com.lily.app.PrettyPrinter;
import com.lily.app.ScopeDef;
import com.lily.app.Compiler;
import com.lily.app.ast.Statement;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import com.lily.app.Parser;

/**
 * Hello world!
 *
 */
public class App 
{

  private static void runCode(String... code) {
    List<Token> tokens = Lexer.lex(String.join("\n", code)).collect(Collectors.toList());
    Statement[] statements = Parser.parse(tokens.listIterator());
    TypeCheck.Type printIntT = new TypeCheck.Type("void", new ArrayList<>(Arrays.asList("int")));
    TypeCheck.Type haltT = new TypeCheck.Type("void", new ArrayList<>());
    HashMap<String, TypeCheck.Type> symbolType = new HashMap<>();
    symbolType.put("printInt", printIntT);
    symbolType.put("halt", haltT);

    TypeCheck typeCheck = new TypeCheck(symbolType);
    try { 
      for (Statement s : statements) {
        s.visit(typeCheck);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    ScopeDef scopeDef = new ScopeDef();
    for (Statement s : statements) {
      s.visit(scopeDef);
    }

    HashMap<String, Integer> interrupts = new HashMap<>();
    interrupts.put("printInt", 1);
    interrupts.put("halt", 0);
    Compiler comp = new Compiler(scopeDef.getGlobals(), scopeDef.getLocals(), interrupts);
    for (Statement s : statements) {
      s.visit(comp);
    }

    ArrayList<Bytecode> finalized = comp.output();
    ByteBuffer buffer = ByteBuffer.allocate(finalized.size() * 4);
    for (Bytecode b : finalized) {
      //System.out.println(b);
      buffer.putInt(Bytecode.encode(b));
    }

    VirtualMachine vm = new VirtualMachine();
    vm.execute(buffer);
  }

  public static void main( String[] args )
  {
    System.out.println("Running code that should print 42");
          runCode("int b = 1;",
          "int f(int a) {",
          "  int c = 2;",
          "  if (a % 2 == 0) {",
          "    return a;",
          "  } else {",
          "    return a * 2;",
          "  }",
          "}",
          "void main(){int a=20;printInt(f(a+b)); halt(); }"
          );

    System.out.println("Running code that should print fibo(10)");
          runCode("int fibo(int n) {",
          "  if (n == 0) {",
          "    return 0;",
          "  } else {",
          "    if (n == 1) {",
          "      return 1;",
          "    } else {",
          "      return fibo(n-1) + fibo(n-2);",
          "    }",
          "  }",
          "  return 0;",
          "}",
          "void main() {",
          "  printInt(fibo(10));",
          "  halt();",
          "}"
          );


  }
}
