package com.lily.app;

import com.lily.app.Bytecode;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class VirtualMachine {

  final int PAGE_SIZE = 4096;

  int pc = 0;
  int sp = 0;

  int stack_top = 0;
  int[] stack = new int[1000];
  HashMap<Integer, ByteBuffer> memory = new HashMap<>();
  int codeSize;

  public VirtualMachine() {
  }

  private void loadCode(ByteBuffer bytecode) {
    int i = 0;
    for (; i < bytecode.limit(); i += PAGE_SIZE) {
      byte[] chunk = new byte[PAGE_SIZE];
      bytecode.get(i, chunk, 0, Math.min(PAGE_SIZE, bytecode.limit() - i));
      memory.put(i, ByteBuffer.wrap(chunk));
    }

    pc = 0;
    sp = bytecode.limit();
    codeSize = bytecode.limit();
  }

  private void store(int address, int value) {
    int chunk_id = address / PAGE_SIZE;
    int offset = address % PAGE_SIZE;
    if (!memory.containsKey(chunk_id)) {
      memory.put(chunk_id, ByteBuffer.allocate(PAGE_SIZE));
    }

    memory.get(chunk_id).putInt(offset, value);
  }

  private int loadInt(int address) {
    int chunk_id = address / PAGE_SIZE;
    int offset = address % PAGE_SIZE;
    if (!memory.containsKey(chunk_id)) {
      memory.put(chunk_id, ByteBuffer.allocate(PAGE_SIZE));
    }

    return memory.get(chunk_id).getInt(offset);
  }

  public void execute(ByteBuffer bytecode) {
    loadCode(bytecode);
    try {
      while (true) {
        //dumpValueStack();
        Bytecode instr = Bytecode.decode(loadInt(pc));
        //System.out.println("PC: " + pc + " " + instr);

        int oldPc = pc;

        switch (instr) {
          case Bytecode.PushImmediate push:
            stack[stack_top++] = push.value;
            pc += 4;
            break;
          case Bytecode.Load load:
          {
            int address = stack[--stack_top];
            stack[stack_top++] = loadInt(address);
            pc += 4;
            break;
          }
          case Bytecode.Store store:
          {
            int value = stack[--stack_top];
            int address = stack[--stack_top];
            store(address, value);
            pc += 4;
            break;
          }
          case Bytecode.Add add:
            int b = stack[--stack_top];
            int a = stack[--stack_top];
            stack[stack_top++] = a + b;
            pc += 4;
            break;
          case Bytecode.Sub sub:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a - b;
            pc += 4;
            break;
          case Bytecode.Mul mul:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a * b;
            pc += 4;
            break;
          case Bytecode.Div div:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a / b;
            pc += 4;
            break;
          case Bytecode.Mod mod:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a % b;
            pc += 4;
            break;
          case Bytecode.Eq eq:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a == b ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.Ne ne:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a != b ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.Lt lt:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a < b ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.Le le:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a <= b ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.Gt gt:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a > b ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.Ge ge:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a >= b ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.And and:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a & b;
            pc += 4;
            break;
          case Bytecode.Or or:
            b = stack[--stack_top];
            a = stack[--stack_top];
            stack[stack_top++] = a | b;
            pc += 4;
            break;
          case Bytecode.Not not:
            a = stack[--stack_top];
            stack[stack_top++] = a == 0 ? 1 : 0;
            pc += 4;
            break;
          case Bytecode.Neg neg:
            a = stack[--stack_top];
            stack[stack_top++] = -a;
            pc += 4;
            break;
          case Bytecode.Dup dup:
            stack[stack_top] = stack[stack_top - 1];
            stack_top++;
            pc += 4;
            break;
          case Bytecode.RelativeBranch branch:
            pc += branch.offset;
            break;
          case Bytecode.ConditionalBranch branch:
            int cond = stack[--stack_top];
            if (cond != 0) {
              pc += branch.offset;
            } else {
              pc += 4;
            }
            break;
            case Bytecode.PushPc pushPc:
            stack[stack_top++] = pc;
            pc += 4;
            break;
          case Bytecode.PopPc popPc:
            pc = stack[--stack_top];
            break;
          case Bytecode.PushSp pushSp:
            stack[stack_top++] = sp;
            pc += 4;
            break;
          case Bytecode.PopSp popSp:
            sp = stack[--stack_top];
            pc += 4;
            break;
          case Bytecode.Interrupt interrupt:
            switch (interrupt.value) {
              case 0:
                // Halt
                return;
              case 1:
                // PrintInt
                System.out.println(stack[--stack_top]);
                break;
              default:
                throw new RuntimeException("Unknown interrupt: " + interrupt.value);
            }
            pc += 4;
            break;
          case Bytecode.Swap swap:
            int tmp = stack[stack_top - 1];
            stack[stack_top - 1] = stack[stack_top - 2];
            stack[stack_top - 2] = tmp;
            pc += 4;
            break;
          case Bytecode.Pop pop:
            stack_top--;
            pc += 4;
            break;
          default:
            throw new RuntimeException("Unknown instruction: " + instr);
        }

        if (pc >= codeSize) {
          int problem = pc;
          pc = oldPc;
          throw new RuntimeException("Program counter out of bounds " + problem);
        }
      }
    } catch (Exception e) {
      System.out.println("PC: " + pc);
      for (int i = Math.max(0, pc-40); i < Math.min(pc+40, codeSize); i+=4) {
        if (i == pc) {
          System.out.print("-> ");
        } else {
          System.out.print("   ");
        }
        System.out.println("Memory[" + i + "]: " + Bytecode.decode(loadInt(i)));
      }
      System.out.println("SP: " + sp);
      System.out.println("Internal Stack Top: " + stack_top);
      throw e;
    }
  }

  private void dumpValueStack() {
    System.out.print("Stack: ");
    for (int i = 0; i < stack_top; i++) {
      System.out.print(stack[i] + " ");
    }
    System.out.println();
  }
}
