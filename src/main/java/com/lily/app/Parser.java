package com.lily.app;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Optional;
import com.lily.app.ast.Statement;
import com.lily.app.ast.Expression;
import org.javatuples.Pair;

public class Parser {
  /**
   * Parse a top level module file
   * @param tokens The tokens to parse
   */
  public static Statement[] parse(ListIterator<Token> tokens) {

    ArrayList<Statement> statements = new ArrayList<Statement>();
    while (tokens.hasNext()) {
      statements.add(parseStatement(tokens));

    }

    return statements.toArray(new Statement[statements.size()]);
  }

  private static Statement parseStatement(ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();

    if (token.toIdentifier().isPresent()) {
      return parseExprOrDecl(token.toIdentifier().get(), tokens);
    } else if (token.toKeyword().isPresent()) {
      return parseKeywordStatement(token.toKeyword().get(), tokens);
    } else {
      return parseExprStatement(token.toNumber().get(), tokens);
    }
  }

  private static Statement parseExprOrDecl(String string, ListIterator<Token> tokens) {
    Token next = nextToken(tokens).get();
    if (next.toIdentifier().isPresent()) {
      return parseDeclaration(string, next.toIdentifier().get(), tokens);
    } else {
      Expression expr = parseExpressionWithPrefix(string, next.toKeyword().get(), tokens);
      next = nextToken(tokens).get();
      if (next.toKeyword().isPresent() && next.toKeyword().get().equals(";")) {
        return new Statement.ExpressionStatement(expr);
      } else if (next.toKeyword().isPresent() && next.toKeyword().get().equals("=")) {
        Expression rvalue = parseExpression(tokens);
        return new Statement.Assignment(expr, rvalue);
      } else {
        throw new RuntimeException("Unexpected token: " + next);
      }
    }
  }

  private static Statement parseDeclaration(String typename, String id, ListIterator<Token> tokens) {
    Token next = nextToken(tokens).get();
    if (next.toKeyword().isPresent() && next.toKeyword().get().equals("=")) {
      Expression expr = parseExpression(tokens);
      checkSkipKeyword(";", tokens);
      return new Statement.Declaration(typename, id, expr);
    } else if (next.toKeyword().isPresent() && next.toKeyword().get().equals("(")) {
      ArrayList<Pair<String, String>> args = new ArrayList<Pair<String,String>>();

      Token t = nextToken(tokens).get();
      if (t.toKeyword().isPresent() && t.toKeyword().get().equals(")")) {
        return new Statement.Function(typename, id, args, parseBlock(tokens));
      }

      while (true) {
        String argType = t.toIdentifier().get();
        String argName = nextToken(tokens).get().toIdentifier().get();
        args.add(Pair.with(argType, argName));

        t = nextToken(tokens).get();
        if (t.toKeyword().isPresent() && t.toKeyword().get().equals(")")) {
          break;
        } else if (t.toKeyword().isPresent() && t.toKeyword().get().equals(",")) {
          t = nextToken(tokens).get();
        } else {
          throw new RuntimeException("Unexpected token: " + t);
        }
      }

      return new Statement.Function(typename, id, args, parseBlock(tokens));
    } else if (next.toKeyword().isPresent() && next.toKeyword().get().equals(";")) {
      return new Statement.Declaration(typename, id, null);
    } else {
      throw new RuntimeException("Unexpected token: " + next);
    }
  }

  private static Expression parseExpressionWithPrefix(String variable, String op, ListIterator<Token> tokens) {
    Expression right;
    switch (op) {
      case "(":
        Expression call = parseCall(new Expression.Variable(variable), tokens);
        return parseLogicPrime(call, tokens);
      case "&&":
      case "||":
        right = parseComparison(tokens);
        return parseLogicPrime(new Expression.Binary(new Expression.Variable(variable), op, right), tokens);
      case "==":
      case "!=":
      case "<=":
      case ">=":
      case "<":
      case ">":
        right = parseArith(tokens);
        return parseComparisonPrime(new Expression.Binary(new Expression.Variable(variable), op, right), tokens);
      case "%":
      case "+":
      case "-":
        right = parseTerm(tokens);
        return parseArithPrime(new Expression.Binary(new Expression.Variable(variable), op, right), tokens);
      case "*":
      case "/":
        right = parseFactor(tokens);
        return parseTermPrime(new Expression.Binary(new Expression.Variable(variable), op, right), tokens);
      default:
        tokens.previous();
        return new Expression.Variable(variable);
    }
  }

    private static Statement parseAssignment(Expression variable, ListIterator<Token> tokens) {
      throw new RuntimeException("Not implemented");
    }

    private static Expression parseCall(Expression variable, ListIterator<Token> tokens) {
      ArrayList<Expression> args = new ArrayList<Expression>();
      while (true) {
        Token t = peekToken(tokens).get();
        if (t.toKeyword().isPresent() && t.toKeyword().get().equals(")")) {
          tokens.next();
          break;
        }

        args.add(parseExpression(tokens));
        t = peekToken(tokens).get();
        if (t.toKeyword().isPresent() && t.toKeyword().get().equals(",")) {
          tokens.next();
        }
      }

      return new Expression.Call(variable, args.toArray(new Expression[args.size()]));
    }

  private static Statement parseExprStatement(int value, ListIterator<Token> tokens) {
    Expression expr = new Expression.Number(value);
    Expression ret = parseLogicPrime(expr, tokens);
    checkSkipKeyword(";", tokens);
    return new Statement.ExpressionStatement(ret);
  }

  private static Statement parseKeywordStatement(String id, ListIterator<Token> tokens) {
    if (id.equals("if")) {
      return parseIf(tokens);
    } else if (id.equals("return")) {
      Expression expr = parseExpression(tokens);
      checkSkipKeyword(";", tokens);
      return new Statement.Return(expr);
    } else if (id.equals("(")) {
      Expression expr = parseParens(tokens);
      Expression ret = parseLogicPrime(expr, tokens);
      checkSkipKeyword(";", tokens);
      return new Statement.ExpressionStatement(ret);
    } else {
      throw new RuntimeException("Unexpected token: " + id);
    }
  }

  private static Expression parseParens(ListIterator<Token> tokens) {
    Expression expr = parseExpression(tokens);
    checkSkipKeyword(")", tokens);
    return expr;
  }

  private static Statement[] parseBlock(ListIterator<Token> tokens) {
    checkSkipKeyword("{", tokens);
    ArrayList<Statement> statements = new ArrayList<Statement>();
    while (tokens.hasNext()) {
      Token t = peekToken(tokens).get();
      if (t.toKeyword().isPresent() && t.toKeyword().get().equals("}")) {
        break;
      }

      statements.add(parseStatement(tokens));
    }

    checkSkipKeyword("}", tokens);

    return statements.toArray(new Statement[statements.size()]);
  }
  private static Statement parseIf(ListIterator<Token> tokens) {
    checkSkipKeyword("(", tokens);
    Expression test = parseExpression(tokens);
    checkSkipKeyword(")", tokens);

    Statement[] consequent = parseBlock(tokens);

    if (nextToken(tokens).get().toKeyword().get().equals("else")) {
      Statement[] alternate = parseBlock(tokens);
      return new Statement.If(test, consequent, alternate);
    } else {
      return new Statement.If(test, consequent, new Statement[0]);
    }
  }

  private static Expression parseExpression(ListIterator<Token> tokens) {
    Expression expr = parseLogic(tokens);
    return expr;
  }

  private static Expression parseLogic(ListIterator<Token> tokens) {
    Expression expr = parseComparison(tokens);
    return parseLogicPrime(expr, tokens);
  }

  private static Expression parseLogicPrime(Expression expr, ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (token.toKeyword().isPresent()) {
      String op = token.toKeyword().get();
      if (op.equals("&&") || op.equals("||")) {
        Expression parseComparison = parseComparison(tokens);
        return parseLogicPrime(new Expression.Binary(expr, op, parseComparison), tokens);
      } else {
        tokens.previous();
        return expr;
      }
    } else {
      return expr;
    }
  }

  private static Expression parseComparison(ListIterator<Token> tokens) {
    Expression expr = parseArith(tokens);
    return parseComparisonPrime(expr, tokens);
  }

  private static Expression parseComparisonPrime(Expression expr, ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (token.toKeyword().isPresent()) {
      String op = token.toKeyword().get();
      if (op.equals("==") || op.equals("!=") || op.equals("<=") || op.equals(">=") || op.equals("<") || op.equals(">") || op.equals("%")) {
        Expression parseExpressionArith = parseArith(tokens);
        return parseComparisonPrime(new Expression.Binary(expr, op, parseExpressionArith), tokens);
      } else {
        tokens.previous();
        return expr;
      }
    } else {
      return expr;
    }
  }

  private static Expression parseArith(ListIterator<Token> tokens) {
    Expression expr = parseTerm(tokens);
    return parseArithPrime(expr, tokens);
  }

  private static Expression parseArithPrime(Expression expr, ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (token.toKeyword().isPresent()) {

      String op = token.toKeyword().get();
      if (op.equals("+") || op.equals("-")) {
        Expression parseTerm = parseTerm(tokens);
        return parseArithPrime(new Expression.Binary(expr, op, parseTerm), tokens);
      } else {
        tokens.previous();
        return expr;
      }
    } else {
      return expr;
    }
  }

  private static Expression parseTerm(ListIterator<Token> tokens) {
    Expression parseFactor = parseFactor(tokens);
    return parseTermPrime(parseFactor, tokens);
  }

  private static Expression parseTermPrime(Expression expr, ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (token.toKeyword().isPresent()) {
      String op = token.toKeyword().get();
      if (op.equals("*") || op.equals("/")) {
        Expression parseFactor = parseFactor(tokens);
        return parseTermPrime(new Expression.Binary(expr, op, parseFactor), tokens);
      } else {
        tokens.previous();
        return expr;
      }
    } else {
      return expr;
    }
  }

  private static Expression parseFactor(ListIterator<Token> tokens) {
    Expression left = parseAtom(tokens);
    Token token = nextToken(tokens).get();
    if (token.toKeyword().isPresent() && token.toKeyword().get().equals("(")) {
      Expression call = parseCall(left, tokens);
      return parseFactorPrime(call, tokens);
    } else {
      tokens.previous();
      return left;
    }
  }

  private static Expression parseFactorPrime(Expression expr, ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (token.toKeyword().isPresent() && token.toKeyword().get().equals("(")) {
      Expression call = parseCall(expr, tokens);
      return parseFactorPrime(call, tokens);
    } else {
      tokens.previous();
      return expr;
    }
  }

  private static Expression parseAtom(ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (token.toNumber().isPresent()) {
      return new Expression.Number(token.toNumber().get());
    } else if (token.toIdentifier().isPresent()) {
      return new Expression.Variable(token.toIdentifier().get());
    } else if (token.toKeyword().isPresent() && token.toKeyword().get().equals("(")) {
      Expression expr = parseExpression(tokens);
      checkSkipKeyword(")", tokens);
      return expr;
    } else {
      throw new RuntimeException("Unexpected token: " + token);
    }
  }

  public static Optional<Token> peekToken(ListIterator<Token> tokens) {
    if (!tokens.hasNext()) {
      return Optional.empty();
    }

    Token token = tokens.next();
    tokens.previous();

    return Optional.of(token);
  }

  public static Optional<Token> nextToken(ListIterator<Token> tokens) {
    if (!tokens.hasNext()) {
      return Optional.empty();
    }

    Token t = tokens.next();

    return Optional.of(t);
  }

  public static void checkSkipKeyword(String kw, ListIterator<Token> tokens) {
    Token token = nextToken(tokens).get();
    if (!token.toKeyword().isPresent() || !token.toKeyword().get().equals(kw)) {
      throw new RuntimeException("Expected keyword: " + kw);
    }
  }
}
