package com.lily.app;

import com.lily.app.Bytecode;
import com.lily.app.Visitor;
import com.lily.app.ast.Expression;
import com.lily.app.ast.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.javatuples.Pair;

public class Compiler implements Visitor {

  public static interface TempBytecode {
    public static class Concrete implements TempBytecode {
      public Bytecode bytecode;
      public Concrete(Bytecode bytecode) {
        this.bytecode = bytecode;
      }
    }

    public static class GlobalOffset implements TempBytecode {
      public String varname;
      public GlobalOffset(String varname) {
        this.varname = varname;
      }
    }

    public static class FunctionOffset implements TempBytecode {
      public String funcname;
      public FunctionOffset(String funcname) {
        this.funcname = funcname;
      }
    }
  }

  ArrayList<TempBytecode> bytecode = new ArrayList<>();
  HashMap<String, Integer> globals;
  HashMap<String, HashMap<String, Integer>> locals;
  HashMap<String, Integer> functions = new HashMap<>();
  HashMap<String, Integer> interrupts;

  String funcName = null;
  boolean lastExprWasInterrupt = false;

  public final HashMap<String, Integer> getFunctions() {
    return functions;
  }



  public Compiler(HashMap<String, Integer> globals, HashMap<String, HashMap<String, Integer>> locals, HashMap<String, Integer> interrupts) {
    this.globals = globals;
    this.locals = locals;
    this.interrupts = interrupts;
  }

  public ArrayList<Bytecode> output() {
    ArrayList<Bytecode> finalized = new ArrayList<>();

    // push jump to main
    finalized.add(new Bytecode.PushImmediate(8 + globals.size() * 4 + functions.get("main")));
    finalized.add(new Bytecode.PopPc());

    // prepare place for globals
    for (int i = 0; i < globals.size(); i++) {
      finalized.add(new Bytecode.PushImmediate(0));
    }

    // push all code
    for (TempBytecode tb : bytecode) {
      switch (tb) {
        case TempBytecode.Concrete concrete:
          finalized.add(concrete.bytecode);
          break;
        case TempBytecode.GlobalOffset go:
          finalized.add(new Bytecode.PushImmediate(8 + globals.get(go.varname) * 4));
          break;
        case TempBytecode.FunctionOffset fo:
          finalized.add(new Bytecode.PushImmediate(8 + globals.size() * 4 + functions.get(fo.funcname)));
          break;
        default:
          throw new RuntimeException("Unknown instruction: " + tb);
      }
    }
    return finalized;
  }

  @Override
  public void visit(Statement.Assignment ass) {
    //ass.lvalue.visit(this);
    // TODO: for now we only compute assignment on variables
    String varname = ((Expression.Variable)ass.lvalue).value;
    addrOf(varname);
    ass.rvalue.visit(this);
    pushInstr(new Bytecode.Store(Bytecode.Width.INT));
  }

  @Override
  public void visit(Expression.Binary bin) {
    bin.left.visit(this);
    bin.right.visit(this);
    switch (bin.op) {
      case "+":
        pushInstr(new Bytecode.Add());
        break;
      case "-":
        pushInstr(new Bytecode.Sub());
        break;
      case "*":
        pushInstr(new Bytecode.Mul());
        break;
      case "/":
        pushInstr(new Bytecode.Div());
        break;
      case "%":
        pushInstr(new Bytecode.Mod());
        break;
      case "==":
        pushInstr(new Bytecode.Eq());
        break;
      case "!=":
        pushInstr(new Bytecode.Ne());
        break;
      case "<":
        pushInstr(new Bytecode.Lt());
        break;
      case "<=":
        pushInstr(new Bytecode.Le());
        break;
      case ">":
        pushInstr(new Bytecode.Gt());
        break;
      case ">=":
        pushInstr(new Bytecode.Ge());
        break;
      case "&&":
        pushInstr(new Bytecode.And());
        break;
      case "||":
        pushInstr(new Bytecode.Or());
        break;
      default:
        throw new RuntimeException("Unknown operator: " + bin.op);
    }

    lastExprWasInterrupt = false;
  }

  @Override
  public void visit(Expression.Number n) {
    pushInstr(new Bytecode.PushImmediate(n.value));

    lastExprWasInterrupt = false;
  }

  private void addrOf(String v) {
    HashMap<String, Integer> local = locals.get(funcName);
    if (local == null) {
      pushGlobalPlaceholder(v);
    } else {
      Integer offset = local.get(v);
      if (offset == null) {
        Integer globalOffset = globals.get(v);
        if (globalOffset == null) {
          throw new RuntimeException("Unknown variable: " + v);
        }
        pushGlobalPlaceholder(v);
      } else {
        pushInstr(new Bytecode.PushSp());
        pushInstr(new Bytecode.PushImmediate(offset * 4));
        pushInstr(new Bytecode.Sub());
      }
    }
  }

  @Override
  public void visit(Expression.Variable v) {
    addrOf(v.value);
    pushInstr(new Bytecode.Load(Bytecode.Width.INT));

    lastExprWasInterrupt = false;
  }

  @Override
  public void visit(Statement.Declaration decl) {
    if (decl.init == null) {
      return;
    }

    new Statement.Assignment(new Expression.Variable(decl.id), decl.init).visit(this);
  }

  @Override
  public void visit(Statement.Function func) {
    funcName = func.id;
    functions.put(funcName, bytecode.size() * 4);

    // push frame on callstack
    pushInstr(new Bytecode.PushSp());
    pushInstr(new Bytecode.PushImmediate(locals.get(funcName).size() * 4));
    pushInstr(new Bytecode.Add());
    pushInstr(new Bytecode.PopSp());

    // store arguments in locals
    for (Pair<String, String> param : func.params) {
      int offset = locals.get(funcName).get(param.getValue1());
      // compute address of locals
      pushInstr(new Bytecode.PushSp());
      pushInstr(new Bytecode.PushImmediate(offset * 4));
      pushInstr(new Bytecode.Sub());
      pushInstr(new Bytecode.Swap());
      // store argument
      pushInstr(new Bytecode.Store(Bytecode.Width.INT));
    }

    for (Statement stmt : func.body) {
      stmt.visit(this);
    }

    funcName = null;
  }

  @Override
  public void visit(Statement.ExpressionStatement expr) {
    expr.expression.visit(this);

    if (!lastExprWasInterrupt) {
      pushInstr(new Bytecode.Pop());
    }
  }

  @Override
  public void visit(Expression.Call call) {
    String funcname = ((Expression.Variable)call.callee).value;
    int immOff = 0;
    if (locals.containsKey(funcname)) {
      pushInstr(new Bytecode.PushPc());
      immOff = bytecode.size() * 4;
      pushInstr(null);
      pushInstr(new Bytecode.Add());
    }

    for (Expression arg : call.args) {
      arg.visit(this);
    }

    if (locals.containsKey(funcname)) {
      // compute address of Function
      pushFunctionPlaceholder(((Expression.Variable)call.callee).value);
      pushInstr(new Bytecode.PopPc());
      bytecode.set(immOff / 4, new TempBytecode.Concrete(new Bytecode.PushImmediate(bytecode.size() * 4 - immOff + 4)));
    } else {
      pushInstr(new Bytecode.Interrupt(interrupts.get(funcname)));
    }

    lastExprWasInterrupt = !locals.containsKey(funcname);
  }

  @Override
  public void visit(Statement.If ite) {
    ite.test.visit(this);
    pushInstr(new Bytecode.Not());

    // push a placeholder for the conditional branch
    int branchOffset = bytecode.size() * 4;
    pushInstr(new Bytecode.ConditionalBranch(0));

    for (Statement stmt : ite.consequent) {
      stmt.visit(this);
    }

    // push a placeholder for the jump over the alternate
    
    int thenEnd = bytecode.size() * 4;
    bytecode.set(branchOffset / 4, new TempBytecode.Concrete(new Bytecode.ConditionalBranch(thenEnd - branchOffset + 4)));

    if (ite.alternate != null) {
      pushInstr(new Bytecode.RelativeBranch(0));
      for (Statement stmt : ite.alternate) {
        stmt.visit(this);
      }

      int elseEnd = bytecode.size() * 4;
      bytecode.set(thenEnd / 4, new TempBytecode.Concrete(new Bytecode.RelativeBranch(elseEnd - thenEnd)));
    }
  }

  @Override
  public void visit(Statement.Return ret) {
    ret.argument.visit(this);
    pushInstr(new Bytecode.PushSp());
    pushInstr(new Bytecode.PushImmediate(locals.get(funcName).size() * 4));
    pushInstr(new Bytecode.Sub());
    pushInstr(new Bytecode.PopSp());
    pushInstr(new Bytecode.Swap());
    pushInstr(new Bytecode.PopPc());
  }

  void pushGlobalPlaceholder(String varname) {
    bytecode.add(new TempBytecode.GlobalOffset(varname));
  }

  void pushInstr(Bytecode instr) {
    bytecode.add(new TempBytecode.Concrete(instr));
  }

  void pushFunctionPlaceholder(String funcname) {
    bytecode.add(new TempBytecode.FunctionOffset(funcname));
  }
}
