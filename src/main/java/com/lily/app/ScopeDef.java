package com.lily.app;

import com.lily.app.ast.Expression;
import com.lily.app.ast.Statement;
import com.lily.app.Visitor;
import org.javatuples.Pair;
import java.util.HashMap;


public class ScopeDef implements Visitor {
  HashMap<String, Integer> globals = new HashMap<>();
  HashMap<String, HashMap<String, Integer>> locals = new HashMap<>();

  String funcName = null;

  public HashMap<String, Integer> getGlobals() {
    return globals;
  }

  public HashMap<String, HashMap<String, Integer>> getLocals() {
    return locals;
  }

  @Override
  public void visit(Statement.Declaration decl) {
    if (funcName == null) {
      globals.put(decl.id, globals.size());
    } else {
      locals.get(funcName).put(decl.id, locals.get(funcName).size());
    }
  }

  @Override
  public void visit(Statement.Function func) {
    funcName = func.id;
    locals.put(funcName, new HashMap<>());
    for (Pair<String, String> param : func.params) {
      locals.get(funcName).put(param.getValue1(), locals.get(funcName).size());
    }
    for (Statement stmt : func.body) {
      stmt.visit(this);
    }
    funcName = null;
  }

  @Override
  public void visit(Statement.Return ret) {
  }

  @Override
  public void visit(Statement.ExpressionStatement expr) {
  }

  @Override
  public void visit(Statement.Assignment ass) {
  }

  @Override
  public void visit(Statement.If ite) {
    for (Statement stmt : ite.consequent) {
      stmt.visit(this);
    }

    if (ite.alternate != null) {
      for (Statement stmt : ite.alternate) {
        stmt.visit(this);
      }
    }
  }

  @Override
  public void visit(Expression.Binary b) {
  }

  @Override
  public void visit(Expression.Variable v) {
  }

  @Override
  public void visit(Expression.Number n) {
  }

  @Override
  public void visit(Expression.Call c) {
  }
}

