package com.lily.app;

import com.lily.app.ast.Statement;
import com.lily.app.ast.Expression;
import com.lily.app.Visitor;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;

public class PrettyPrinter implements Visitor {

  Writer writer;
  int indent = 0;

  public PrettyPrinter(Writer writer) {
    this.writer = writer;
  }

  @Override
  public final void visit(Statement.If statement) {
    try {
      writer.write("if (");
      statement.test.visit(this);
      writer.write(") {\n");
      indent++;
      for (Statement s : statement.consequent) {
        indent();
        s.visit(this);
        writer.write("\n");
      }
      indent--;
      indent();
      writer.write("}");
      if (statement.alternate != null) {
        writer.write(" else {\n");
        indent++;
        for (Statement s : statement.alternate) {
          indent();
          s.visit(this);
          writer.write("\n");
        }
        indent--;
        indent();
        writer.write("}");
      }
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Statement.Return statement) {
    try {
      writer.write("return ");
      statement.argument.visit(this);
      writer.write(";");
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Statement.ExpressionStatement statement) {
    try {
      statement.expression.visit(this);
      writer.write(";");
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Statement.Declaration statement) {
    try {
      writer.write(statement.type + " " + statement.id);
      if (statement.init != null) {
        writer.write(" = ");
        statement.init.visit(this);
      }
      writer.write(";");
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Statement.Function statement) {
    try {
      writer.write(statement.returnType + " " + statement.id + "(");
      for (int i = 0; i < statement.params.size(); i++) {
        writer.write(statement.params.get(i).getValue0() + " " + statement.params.get(i).getValue1());
        if (i < statement.params.size() - 1) {
          writer.write(", ");
        }
      }
      writer.write(") {\n");
      indent++;
      for (Statement s : statement.body) {
        indent();
        s.visit(this);
        writer.write("\n");
      }
      indent--;
      indent();
      writer.write("}");
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Expression.Binary expression) {
    try {
      expression.left.visit(this);
      writer.write(" " + expression.op + " ");
      expression.right.visit(this);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Expression.Number expression) {
    try {
      writer.write(String.format("%d", expression.value));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Expression.Variable expression) {
    try {
      writer.write(expression.value);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Expression.Call expression) {
    try {
      expression.callee.visit(this);
      writer.write("(");
      for (int i = 0; i < expression.args.length; i++) {
        expression.args[i].visit(this);
        if (i < expression.args.length - 1) {
          writer.write(", ");
        }
      }
      writer.write(")");
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private void indent() {
    try {
      for (int i = 0; i < indent; i++) {
        writer.write("  ");
      }
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public final void visit(Statement.Assignment assignment) {
    try {
      assignment.lvalue.visit(this);
      writer.write(" = ");
      assignment.rvalue.visit(this);
      writer.write(";");
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
