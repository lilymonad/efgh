package com.lily.app;

import com.lily.app.ast.Statement;
import com.lily.app.ast.Expression;
import com.lily.app.Visitor;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import org.javatuples.Pair;

public class TypeCheck implements Visitor {

  public static class Type {
    String returnType;
    ArrayList<String> params;

    public Type(String returnType, ArrayList<String> params) {
      this.returnType = returnType;
      this.params = params;
    }
  }

  HashMap<String, Type> symbolType = new HashMap<>();
  Type expectedReturnType;
  Type retT;

  public TypeCheck(HashMap<String, Type> symbolType) {
    this.symbolType = symbolType;
  }

  @Override
  public final void visit(Statement.If statement) {
    statement.test.visit(this);
    if (!retT.returnType.equals("bool")) {
      throw new RuntimeException("Type error: if statement test must be of type bool");
    }

    for (Statement s : statement.consequent) {
      s.visit(this);
    }

    if (statement.alternate != null) {
      for (Statement s : statement.alternate) {
        s.visit(this);
      }
    }
  }

  @Override
  public final void visit(Statement.Return statement) {
    statement.argument.visit(this);
    if (!retT.returnType.equals(expectedReturnType.returnType)) {
      throw new RuntimeException("Type error: return statement must return a value of type " + expectedReturnType.returnType + " but got " + retT.returnType);
    }
  }

  @Override
  public final void visit(Statement.ExpressionStatement statement) {
    statement.expression.visit(this);
  }

  @Override
  public final void visit(Statement.Declaration statement) {
    if (symbolType.containsKey(statement.id)) {
      throw new RuntimeException("Type error: variable " + statement.id + " already declared");
    }

    statement.init.visit(this);
    if (!retT.returnType.equals(statement.type)) {
      throw new RuntimeException("Type error: variable " + statement.id + " must be of type " + statement.type);
    }
    symbolType.put(statement.id, retT);
  }

  @Override
  public final void visit(Statement.Function statement) {
    if (symbolType.containsKey(statement.id)) {
      throw new RuntimeException("Type error: function " + statement.id + " already declared");
    }

    HashMap<String, Type> oldSymbolType = symbolType;
    symbolType = new HashMap<>(symbolType);

    Type oldExpected = expectedReturnType;
    expectedReturnType = new Type(statement.returnType, null);

    ArrayList<String> params = new ArrayList<>();
    for (Pair<String, String> p : statement.params) {
      symbolType.put(p.getValue1(), new Type(p.getValue0(), null));
      params.add(p.getValue0());
    }
    symbolType.put(statement.id, new Type(statement.returnType, params));

    for (Statement s : statement.body) {
      s.visit(this);
    }

    expectedReturnType = oldExpected;
    symbolType = oldSymbolType;
    symbolType.put(statement.id, new Type(statement.returnType, params));
  }

  @Override
  public final void visit(Expression.Binary expression) {
    expression.left.visit(this);
    Type left = retT;
    expression.right.visit(this);
    Type right = retT;

    switch (expression.op) {
      case "+":
      case "-":
      case "*":
      case "/":
      case "%":
        if (!(left.returnType.equals("int") && right.returnType.equals("int"))) {
          throw new RuntimeException("Type error: binary operation " + expression.op + " must be applied to two integers");
        }
        retT = new Type("int", null);
        break;
      case "==":
      case "!=":
      case "<=":
      case ">=":
      case "<":
      case ">":
        if (!left.returnType.equals(right.returnType)) {
          throw new RuntimeException("Type error: binary operation " + expression.op + " must be applied to two values of the same type");
        }
        retT = new Type("bool", null);
        break;
      case "&&":
      case "||":
        if (!(left.returnType.equals("bool") && right.returnType.equals("bool"))) {
          throw new RuntimeException("Type error: binary operation " + expression.op + " must be applied to two booleans");
        }
        retT = new Type("bool", null);
        break;
      default:
        throw new RuntimeException("Type error: unknown binary operation " + expression.op);
    }
  }

  @Override
  public final void visit(Expression.Number expression) {
    retT = new Type("int", null);
  }

  @Override
  public final void visit(Expression.Variable expression) {
    if (!symbolType.containsKey(expression.value)) {
      throw new RuntimeException("Type error: variable " + expression.value + " not declared");
    }
    retT = symbolType.get(expression.value);
  }

  @Override
  public final void visit(Expression.Call expression) {
    Expression.Variable callee = (Expression.Variable) expression.callee;
    if (callee == null) {
      throw new RuntimeException("Type error: callee must be a variable");
    }
    if (!symbolType.containsKey(callee.value)) {
      throw new RuntimeException("Type error: function " + callee.value + " not declared");
    }
    Type f = symbolType.get(callee.value);
    if (f.params.size() != expression.args.length) {
      throw new RuntimeException("Type error: function " + callee.value + " expects " + f.params.size() + " arguments");
    }
    for (int i = 0; i < f.params.size(); i++) {
      expression.args[i].visit(this);
      if (!retT.returnType.equals(f.params.get(i))) {
        throw new RuntimeException("Type error: function " + expression.callee + " expects argument " + i + " to be of type " + f.params.get(i));
      }
    }
    retT = f;
  }

  @Override
  public final void visit(Statement.Assignment assignment) {
    // TODO: when we have structs in the language, we need to check that the lvalue is not only a variable, but also a field of a struct
    Expression.Variable lvalue = (Expression.Variable) assignment.lvalue;
    if (lvalue == null || !symbolType.containsKey(lvalue.value)) {
      throw new RuntimeException("Type error: variable " + lvalue.value + " not declared");
    }
    assignment.rvalue.visit(this);
    if (!retT.returnType.equals(symbolType.get(lvalue.value).returnType)) {
      throw new RuntimeException("Type error: variable " + lvalue.value + " must be of type " + symbolType.get(lvalue.value).returnType);
    }
  }
}
