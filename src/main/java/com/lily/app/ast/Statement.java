package com.lily.app.ast;
import java.util.ArrayList;
import org.javatuples.Pair;
import com.lily.app.Visitor;

public interface Statement {
  public static class If implements Statement {
    public Expression test;
    public Statement[] consequent;
    public Statement[] alternate;
    public If(Expression test, Statement[] consequent, Statement[] alternate) {
      this.test = test;
      this.consequent = consequent;
      this.alternate = alternate;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Return implements Statement {
    public Expression argument;
    public Return(Expression argument) {
      this.argument = argument;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class ExpressionStatement implements Statement {
    public Expression expression;
    public ExpressionStatement(Expression expression) {
      this.expression = expression;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Declaration implements Statement {
    public String type;
    public String id;
    public Expression init;
    public Declaration(String type, String id, Expression init) {
      this.type = type;
      this.id = id;
      this.init = init;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Function implements Statement {
    public String returnType;
    public String id;
    public ArrayList<Pair<String, String>> params;
    public Statement[] body;
    public Function(String returnType, String id, ArrayList<Pair<String, String>> params, Statement[] body) {
      this.returnType = returnType;
      this.id = id;
      this.params = params;
      this.body = body;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Assignment implements Statement {
    public Expression lvalue;
    public Expression rvalue;
    public Assignment(Expression lvalue, Expression rvalue) {
      this.lvalue = lvalue;
      this.rvalue = rvalue;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public abstract void visit(Visitor visitor);
}
