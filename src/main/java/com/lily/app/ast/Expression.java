package com.lily.app.ast;

import com.lily.app.Visitor;

public interface Expression {
  public static class Number implements Expression {
    public int value;
    public Number(int value) {
      this.value = value;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Variable implements Expression {
    public String value;
    public Variable(String value) {
      this.value = value;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Binary implements Expression {
    public Expression left;
    public Expression right;
    public String op;
    public Binary(Expression left, String op, Expression right) {
      this.left = left;
      this.op = op;
      this.right = right;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  public static class Call implements Expression {
    public Expression callee;
    public Expression[] args;
    public Call(Expression callee, Expression[] args) {
      this.callee = callee;
      this.args = args;
    }

    @Override
    public final void visit(Visitor visitor) {
      visitor.visit(this);
    }
  }

  abstract void visit(Visitor visitor);
}
