package com.lily.app;

import java.nio.ByteBuffer;

public interface Bytecode {

  public enum Width {
    BYTE(1),
    SHORT(2),
    INT(4);

    public final int size;

    Width(int size) {
      this.size = size;
    }
  }

  public static Bytecode decode(int instruction) {
    byte opcode = (byte)(instruction & 0xff);
    int operand = instruction >> 8;

    switch (opcode) {
      case 0x01:
        return new PushImmediate(operand);
      case 0x02:
        switch (operand) {
          case 1:
            return new Load(Width.BYTE);
          case 2:
            return new Load(Width.SHORT);
          default:
            return new Load(Width.INT);
        }
      case 0x03:
        switch (operand) {
          case 1:
            return new Store(Width.BYTE);
          case 2:
            return new Store(Width.SHORT);
          default:
            return new Store(Width.INT);
        }
      case 0x04:
        return new Add();
      case 0x05:
        return new Sub();
      case 0x06:
        return new Mul();
      case 0x07:
        return new Div();
      case 0x08:
        return new Mod();
      case 0x09:
        return new Eq();
      case 0x0A:
        return new Ne();
      case 0x0B:
        return new Lt();
      case 0x0C:
        return new Le();
      case 0x0D:
        return new Gt();
      case 0x0E:
        return new Ge();
      case 0x0F:
        return new And();
      case 0x10:
        return new Or();
      case 0x11:
        return new Not();
      case 0x12:
        return new Neg();
      case 0x13:
        return new Dup();
      case 0x14:
        return new RelativeBranch(operand);
      case 0x15:
        return new ConditionalBranch(operand);
      case 0x16:
        return new PushPc();
      case 0x17:
        return new PopPc();
      case 0x18:
        return new PushSp();
      case 0x19:
        return new PopSp();
      case 0x1A:
        return new Interrupt(operand);
      case 0x1B:
        return new Swap();
      case 0x1C:
        return new Pop();
      default:
        throw new RuntimeException("Unknown instruction: " + opcode);
    }
  }

  abstract int immediate();
  abstract byte opcode();

  public static int encode(Bytecode bytecode) {
    return bytecode.immediate() << 8 | bytecode.opcode();
  }

  public static class PushImmediate implements Bytecode {
    public final int value;

    public PushImmediate(int value) {
      this.value = value;
    }

    public int immediate() {
      return value;
    }

    public byte opcode() {
      return 0x01;
    }

    @Override
    public String toString() {
      return "push " + value;
    }
  }

  public static class Load implements Bytecode {
    public final Width width;

    public Load(Width width) {
      this.width = width;
    }

    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x02;
    }

    @Override
    public String toString() {
      return "load";
    }
  }

  public static class Store implements Bytecode {
    public final Width width;

    public Store(Width width) {
      this.width = width;
    }

    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x03;
    }

    @Override
    public String toString() {
      return "store";
    }
  }

  public static class Add implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x04;
    }

    @Override
    public String toString() {
      return "add";
    }
  }

  public static class Sub implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x05;
    }

    @Override
    public String toString() {
      return "sub";
    }
  }

  public static class Mul implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x06;
    }

    @Override
    public String toString() {
      return "mul";
    }
  }

  public static class Div implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x07;
    }

    @Override
    public String toString() {
      return "div";
    }
  }

  public static class Mod implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x08;
    }

    @Override
    public String toString() {
      return "mod";
    }
  }

  public static class Eq implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x09;
    }

    @Override
    public String toString() {
      return "eq";
    }
  }

  public static class Ne implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x0A;
    }

    @Override
    public String toString() {
      return "ne";
    }
  }

  public static class Lt implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x0B;
    }

    @Override
    public String toString() {
      return "lt";
    }
  }

  public static class Le implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x0C;
    }

    @Override
    public String toString() {
      return "le";
    }
  }

  public static class Gt implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x0D;
    }

    @Override
    public String toString() {
      return "gt";
    }
  }

  public static class Ge implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x0E;
    }

    @Override
    public String toString() {
      return "ge";
    }
  }

  public static class And implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x0F;
    }

    @Override
    public String toString() {
      return "and";
    }
  }

  public static class Or implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x10;
    }

    @Override
    public String toString() {
      return "or";
    }
  }

  public static class Not implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x11;
    }

    @Override
    public String toString() {
      return "not";
    }
  }

  public static class Neg implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x12;
    }

    @Override
    public String toString() {
      return "neg";
    }
  }

  public static class Dup implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x13;
    }

    @Override
    public String toString() {
      return "dup";
    }
  }

  public static class RelativeBranch implements Bytecode {
    public final int offset;

    public RelativeBranch(int offset) {
      this.offset = offset;
    }

    public int immediate() {
      return offset;
    }

    public byte opcode() {
      return 0x14;
    }

    @Override
    public String toString() {
      return "j " + offset;
    }
  }

  public static class ConditionalBranch implements Bytecode {
    public final int offset;

    public ConditionalBranch(int offset) {
      this.offset = offset;
    }

    public int immediate() {
      return offset;
    }

    public byte opcode() {
      return 0x15;
    }

    @Override
    public String toString() {
      return "jif " + offset;
    }
  }

  public static class PushPc implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x16;
    }

    @Override
    public String toString() {
      return "push pc";
    }
  }

  public static class PopPc implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x17;
    }

    @Override
    public String toString() {
      return "pop pc";
    }
  }

  public static class PushSp implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x18;
    }

    @Override
    public String toString() {
      return "push sp";
    }
  }

  public static class PopSp implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x19;
    }

    @Override
    public String toString() {
      return "pop sp";
    }
  }

  public static class Interrupt implements Bytecode {
    public final int value;

    public Interrupt(int value) {
      this.value = value;
    }

    public int immediate() {
      return value;
    }

    public byte opcode() {
      return 0x1A;
    }

    @Override
    public String toString() {
      return "interrupt " + value;
    }
  }

  public static class Swap implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x1B;
    }

    @Override
    public String toString() {
      return "swap";
    }
  }

  public static class Pop implements Bytecode {
    public int immediate() {
      return 0;
    }

    public byte opcode() {
      return 0x1C;
    }

    @Override
    public String toString() {
      return "pop";
    }
  }
}
