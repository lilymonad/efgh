package com.lily.app;
import java.util.Optional;

public class Token {
  static private Eof eof = new Eof();
  public static Token newEof() {
    return eof;
  }

  public static Token newNumber(int value) {
    return new Number(value);
  }

  public static Token newKeyword(String value) {
    return new Keyword(value);
  }

  public static Token newIdentifier(String value) {
    return new Identifier(value);
  }

  public Optional<Integer> toNumber() {
    return Optional.empty();
  }

  public Optional<String> toKeyword() {
    return Optional.empty();
  }

  public Optional<String> toIdentifier() {
    return Optional.empty();
  }

  public boolean isEof() {
    return false;
  }
}

class Eof extends Token {
  public Eof() {
    super();
  }

  public boolean isEof() {
    return true;
  }

  public String toString() {
    return "Eof";
  }
}

class Number extends Token {
  int value;
  public Number(int value) {
    super();
    this.value = value;
  }

  public Optional<Integer> toNumber() {
    return Optional.of(value);
  }

  public String toString() {
    return "Number(" + value + ")";
  }
}

class Keyword extends Token {
  String value;
  public Keyword(String value) {
    super();
    this.value = value;
  }

  public Optional<String> toKeyword() {
    return Optional.of(value);
  }

  public String toString() {
    return "Keyword(" + value + ")";
  }
}

class Identifier extends Token {
  String value;
  public Identifier(String value) {
    super();
    this.value = value;
  }

  public Optional<String> toIdentifier() {
    return Optional.of(value);
  }

  public String toString() {
    return "Identifier(" + value + ")";
  }
}

